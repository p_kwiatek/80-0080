<?php
if ($showClass)
{
    ?>
    <h2 class="heading"><?php echo $pageName . ' ' . __('for') . ' ' . $outRowTT['class']?></h2>
    <div class="timetable__info"><?php echo $outRowTT['text']?></div>
    <?php
    foreach($arrSchoolWeek as $k => $v)
    {
	?>
	<div class="timetable__holder table-responsive">
		<table class="timetable">
		    <caption><?php echo $v['long']?></caption>
		    <tr>
				<th width="7%"><?php echo __('no'); ?></th>
	            <th width="12%"><?php echo __('hour'); ?></th>
	            <th width="30%"><?php echo __('lesson'); ?></th>
	            <th width="35%"><?php echo __('teacher'); ?></th>
	            <th width="16%"><?php echo __('room'); ?></th>
		    </tr>
		    <?php
		    $countHour = 0;
		    for($i=0; $i<10; $i++)
		    {
			if (trim($dayPlan[$v['short']][$i]) != '')
			{
			    $countHour++;
			    ?>
			    <tr>
				<td class="planLp"><?php echo ($i+1)?>.</td>
				<td class="planHour"><?php echo $hours[$v['short']][$i]?></td>
				<td class="planLesson"><?php echo $dayPlan[$v['short']][$i]?></td>
				<td class="planTeacher"><?php echo $teacher[$v['short']][$i]?></td>
				<td class="planRoom"><?php echo $room[$v['short']][$i]?></td>
			    </tr>
			    <?php
			}
		    }
						
		    if ($countHour <= 0)
		    {
			?>
			<tr><td colspan="5"><div class="txt_err"><?php echo __('no lessons'); ?></div></td></tr>
			<?php
		    }
		    ?>
		</table>
	</div>
    <?php
    }
	
} else if ($numRows > 0)
{
    ?>
    <h2 class="heading"><?php echo $pageName?></h2>
    <ul class="timetable--classlist">
    <?php
    foreach ($outRowTT as $row)
    {	
	?>
	<li>
	    <a href="plan_lekcji_<?php echo $row['id']?>"><?php echo $row['class']?></a>
	</li>
	<?php
    }
    ?>
    </ul>
    <?php
}
?>