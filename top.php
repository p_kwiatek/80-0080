<div id="popup"></div>
<ul class="skips">
    <li><a href="#skip_tm"><?php echo __('skip to main menu')?></a></li> 
    <li><a href="#skip_mg"><?php echo __('skip to additional menu')?></a></li> 
    <li><a href="#content_txt"><?php echo __('skip to content')?></a></li>
    <li><a href="#skip_srch"><?php echo __('skip to search')?></a></li>
    <li><a href="mapa_strony"><?php echo __('sitemap')?></a></li>
</ul>
<?php
include_once ( CMS_TEMPL . DS . 'toolbar.php');
?>
<header class="hero">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
            	<div class="hero__address">
					<?php
					    echo $headerAddress;
					    if ($pageInfo['email'] != '')
					    {
						echo '<a href="mailto:'.$pageInfo['email'].'">'.$pageInfo['email'].'</a>';
					    }	    
				    ?>
				</div>
			    <div class="hero__name">
			    	<h1>
        				<?php echo word_wrap( array(' w', ' im'), $pageInfo['name']); ?>	
			    	</h1>
			    </div>
                <div class="hero__background hero__background--left"></div>
				<div class="hero__holder">
                    <div class="hero__carousel">
                        <div>
                        	<?php if ($outBannerTopRows == 0): ?>
                                <?php
                                    $img = $templateDir . '/' . 'images' . '/' . 'banner-default.jpg';
                                    $pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
                                    echo '<div class="hero__slide" style="background-image: url('. $img .');"></div>';
                                ?>       
                            <?php elseif ($outBannerTopRows == 1): ?>
                                <?php
                                    $value = $outBannerTop[0];
                                    $pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
                                    $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . rawurlencode($value['photo']);
                                    echo '<div class="hero__slide" style="background-image: url('. $img .');"></div>';
                                ?>
                            <?php else: ?>
                                <?php foreach ($outBannerTop as $value): ?>
                                    <?php
                                        $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . rawurlencode($value['photo']);
                                        echo '<div class="hero__slide" style="background-image:url('. $img .');"></div>';
                                    ?>
                                <?php endforeach ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <div class="hero__background hero__background--right"></div>
            </div>
		</div>
	</div>
</header>
