<?php
if ($pagination['end'] > 1)
{
    ?>
    <div class="pagination">
    <?php
    $active_page = $pagination['active'];
		
    if ( !isset($pagination['active']) )
    {
	$active_page = 1;
    }
    ?>
    <p><?php echo __('page')?> <strong><?php echo $active_page?></strong>/<?php echo $pagination['end']?></p>
    <ul>
    <?php
    if ($pagination['start'] != $pagination['prev'])
    {
	?>
	<li><a href="<?php echo $url . $pagination['start']?>" rel="nofollow" class="btnStart">&lt;&lt;<span class="sr-only"><?php echo __('first page'); ?></span></a></li>
	<li><a href="<?php echo $url . $pagination['prev']?>" rel="nofollow" class="btnPrev">&lt;<span class="sr-only"><?php echo __('prev page'); ?></span></a></li>
	<?php
    }
		
    foreach ($pagination as $k => $v)
    {
	if (is_numeric($k))
	{
	    ?>
	    <li><a href="<?php echo $url . $v?>" rel="nofollow" class="btnPage"><span class="sr-only"><?php echo __('page'); ?> </span><?php echo $v?></a></li>
	    <?php
	} else if ($k == 'active')
	{
	    ?>
	    <li class="pageActive"><a href="#"><span class="sr-only"><?php echo __('page'); ?> </span><?php echo $v?></a></li>
	    <?php
	}			
    }
		
    if ($pagination['active'] != $pagination['end'])
    {
	?>
	<li><a href="<?php echo $url . $pagination['next']?>" rel="nofollow" class="btnNext">&gt;<span class="sr-only"><?php echo __('next page'); ?></span></a></li>
	<li><a href="<?php echo $url . $pagination['end']?>" rel="nofollow" class="btnEnd">&gt;&gt;<span class="sr-only"><?php echo __('last page'); ?></span></a></li>
	<?php
    }
    ?>
    </ul>
    </div>
    <?php
}
?>