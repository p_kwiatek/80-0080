<?php
if ($showAll)
{
    ?>
    <h2><?php echo $pageName?></h2>
    <ul class="gallery">
    <?php
    if (count($albums) > 0)
    {
	$n = 0;
	foreach ($albums as $value)
	{
	    $n++;
	    $noMargin = '';
	    if ($n == $pageConfig['zawijaj'])
	    {
		$noMargin = ' noMargin';
	    }
	    ?>
	    <li class="photoWrapperGallery">
			<a href="<?php echo $value['link']?>" class="photo">
		    	<img src="files/<?php echo $lang?>/mini/<?php echo $value['file']?>" alt="<?php echo $value['name']?>"/>
			</a>
			<p><?php echo $value['name']?></p>
	    </li>
	    <?php
	    if ($n == $pageConfig['zawijaj']){
		$n = 0;
		?>
		<?php
	    }
	}
	
    } else
    {
	echo __('no photo album added');
    }
    ?>
    </ul>
    <?php
}
if ($showOne)
    {
    ?>
    <h2><?php echo $pageName?></h2>
    <?php 
    echo $message;
    ?>
    <ul class="gallery">
	<?php 
	if ($showGallery)
	{
	    if (count($outRows) > 0)
	    {
		$n = 0;
		foreach ($outRows as $value)
		{
		    $n++;
		    $noMargin = '';
		    if ($n == $pageConfig['zawijaj'])
		    {
			$noMargin = ' noMargin';
		    }
		    ?>
		    <li class="photoWrapperGallery">
				<a href="files/<?php echo $lang?>/<?php echo $value['file']?>" rel="fancybox" title="<?php echo $value['name']?>" class="photo">
				    <img src="files/<?php echo $lang?>/mini/<?php echo $value['file']?>" alt="<?php echo __('enlarge image'); ?>" />
				</a>
			<?php
			if (! check_html_text($value['name'], '') )
			{
			    ?>
			    <p><?php echo $value['name']?></p>
			    <?php
			}
		    ?>
		    </li>
		    <?php
		    if ($n == $pageConfig['zawijaj'])
		    {
			$n = 0;
			?>
			<?php
		    }
		}
	    }
	}
   
    if ($showLoginForm)
    {
	include( CMS_TEMPL . DS . 'form_login.php');
    }
    ?>
    </ul>
    <?php
}
?>
