<?php 
// konfiguracja strony
$pageConfig['zawijaj']	    = 4;
$pageConfig['swfWidth']	    = 456;
$pageConfig['swfHeight']    = 285;
$pageConfig['duration']	    = 2;
$pageConfig['transition']   = 1;  

//konfiguracja template
$templateConfig = array(
    'name'	    => '80-0080',
    'column'	    => 2,
    'modulesTop'    => 4,
    'modulesTop2'   => 3,
    'modulesBottom' => 3,
    'modulesRight'  => false,
    'userInModule'  => true,    
    'mainColor'	    => '#346817',
    'highColor'	    => '#ffd200',
    'overColor'	    => '#eff9fa',
    'mainColor-bw'  => '#3f3f3f',
    'highColor-bw'  => '#dedede',
    'overColor-bw'  => '#efefef',
    'mainColor-ct'  => '#ffff00',
    'highColor-ct'  => '#111111',
    'overColor-ct'  => '#000000',
    'clockWidth'    => 54,
    'clockHeight'   => 54,
    'maxWidthLeftAdv'   => 180,
    'maxWidthTopAdv'    => 700,
    'popupBackground-bw'=> '#efefef',
    'popupBackground-ct'=> '#000000',
    'menuLong'	    => 30,
    'menuTopIcons' => true,
    'noFlash' => true
);

//konfiguracja obrazków
$imageConfig = array(
    'maxWidth'	    => 900,
    'miniWidth'	    => 350,
    'miniHeight'    => 220,
    'proportional'  => 0,
    'jpgCompression'=> 95,
    'bgColor'	    => 'EFF9FA',
    'bannerWidth'   => 1170,
    'bannerHeight'  => 550,
    'avatarWidth'   => 50,
    'avatarHeight'  => 50,
    'avatarSize'    => 512000
);
	
$animType = array(
    'Przenikanie'   => 'fade',
    'Przenikanie z powiększeniem' => 'fadeUp',
    'Przenikanie z pomniejszeniem' => 'fadeDown',
    'Nasunięcie z góry' => 'goDown',
    'Nasunięcie z dołu' => 'goUp',
    'Przesunięcie w lewo' => 'backSlide'
);
?>