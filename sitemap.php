<h2><?php echo $pageName; ?></h2>
<div class="sitemap__wrapper">
<?php
foreach ($menuType as $k) 
{
    if ($k['active'] == 1)
    {
	?>
	<div class="sitemap__holder">
		<h3><?php echo $k['name']?></h3>
		<?php
	    	get_menu_tree ($k['menutype'], 0, 0, 'sitemap');
    	}
    	?>
    </div>
    <?php
}		
?>
</div>