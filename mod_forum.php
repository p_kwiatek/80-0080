<?php
/**
 * logowanie
 */
if ($loginForm){
    echo '<a name="logowanie" id="logowanie"></a>';
    echo '<h2>' . $pageName . '</h2>';
    ?>
    <form name="loginForm" id="loginForm" class="" method="post" action="<?php echo $url; ?>,zaloguj#logowanie">
		<div class="form">
		    <?php
		    echo $message;
		    ?>
		    <h3><?php echo __('login action'); ?></h3>
		    <div class="group">
		    	<label for="login"><?php echo __('login'); ?>:</label>
		    	<input type="text" id="login" name="login" class="inText" size="35" maxlength="50" />
		    	<span id="loginError" class="msgMarg"></span>
		    </div>
		    
		    <div class="group">
		    	<label for="password"><?php echo __('password'); ?>:</label>
		    	<input type="password" id="password" name="password" class="inText" size="35" maxlength="50" />
		    	<span id="passwordError" class="msgMarg"></span>
		    </div>
		    
		    <input type="hidden" name="forumLogin" value="1" />
		    
		    <div class="group">
		    	<button type="submit" class="btnForm" name="ok"><?php echo __('login action'); ?></button>
		    </div>
		    <div class="group">
		    	<a href="generuj-haslo" class="button"><?php echo __('password forgot'); ?></a>	    
		    </div>
		    
		</div>	
    </form>

<script type="text/javascript">
// <![CDATA[
    $(document).ready(function() {
	var form = $('#loginForm');
	form.submit(function(){
	    if (validateLogin() & validatePass()){
		//return true;
	    } else {
		//return true;
	    }
	});
	$('#login').blur(validateLogin);
	function validateLogin(){
	    var value = $('#login').val();
	   
	    if (value.length < 4){
		$('#login').addClass('inError');
		$('#loginError').addClass('msgError').text('<?php echo __('error min length login'); ?>');
		return false;
	    } else {
		$('#login').removeClass('inError');
		$('#loginError').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#password').blur(validatePass);
	function validatePass(){
	    var value = $('#password').val();
	    if (value.length < 8 || value.length > 50){
		$('#password').addClass('inError');
		$('#passwordError').addClass('msgError').text('<?php echo __('error min length password'); ?>');
		return false;
	    } else {
		$('#password').removeClass('inError');
		$('#passwordError').text('');
		return true;
	    } 
	}	
    });
// ]]>	
</script>
    <?php
}
/**
 * rejestracja powiodła się
 */
if ($registerSuccess){
    echo '<h2>' . $pageName . '</h2>';
    echo $message;
    ?>
    <ul id="backLinks">
	<li><a href="forum" class="backLink"><?php echo __('forum home page'); ?></a></li>
	<li><a href="index.php" class="backLink"><?php echo __('home page'); ?></a></li>
    </ul>
    <?php
}
/**
 * formularz rejestracji
 */
if ($registerForm){
    echo '<a name="rejestracja" id="rejestracja"></a>';
    echo '<h2>' . $pageName . '</h2>';
    ?>
    <form name="registerForm" id="registerForm" class="jNice" method="post" action="<?php echo $url; ?>,rejestruj#rejestracja" enctype="multipart/form-data">
		<div class="form">
		    <?php
		    echo $message;
		    ?>
		    <h3><?php echo __('register action'); ?></h3>
		    
		    <div class="group">
		    	<label for="login"><?php echo __('login'); ?>:</label>
		    	<input type="text" id="login" name="login" class="inText" size="35" maxlength="50" value="<?php echo $login; ?>" /><span id="loginError" class="msgMarg">
		    </div>
		    
		    <div class="group">
		    	<label for="password"><?php echo __('password'); ?>:</label>
			    <input type="password" id="password" name="password" class="inText" size="35" maxlength="50" value="<?php echo $password; ?>" />
		    	<span id="passwordError" class="msgMarg"></span>
		    </div>
		    
		    <div class="group">
		    	<label for="password_confirm"><?php echo __('repeat password'); ?>:</label>
			    <input type="password" id="password_confirm" name="password_confirm" class="inText" size="35" maxlength="50" value="<?php echo $password_confirm; ?>" />
			    <span id="passwordConfirmError" class="msgMarg"></span>
			</div>	    
		    
		    <div class="group">
		    	<label for="email"><?php echo __('email'); ?>:</label>
		    	<input type="text" id="email" name="email" class="inText" size="35" maxlength="100" value="<?php echo $email; ?>" />
		    	<span id="emailError" class="msgMarg"></span>
		    </div>
		    
		    <div class="group">
		    	<label for="first_name"><?php echo __('firstname'); ?>:</label>
		    	<input type="text" id="first_name" name="first_name" class="inText" size="35" maxlength="50" value="<?php echo $first_name; ?>" />
		    </div>
		    
		    <div class="group">
		    	<label for="last_name"><?php echo __('lastname'); ?>:</label>
		    	<input type="text" id="last_name" name="last_name" class="inText" size="35" maxlength="50" value="<?php echo $last_name; ?>" />
	    	</div>
		    
		    <div class="group">
		    	<label><?php echo __('gender'); ?>:</label>
			    <div class="radio">
					<label for="sex_m" id="l_sex_m"><?php echo __('man'); ?></label>
					<input type="radio" id="sex_m" name="sex" value="m" checked="checked" />
				</div>
				<div class="radio">
					<label for="sex_f" id="l_sex_f"><?php echo __('woman'); ?></label>
					<input type="radio" id="sex_f" name="sex" value="f" />
				</div>
		    </div>
		    
		    <div class="group">
		    	<label for="avatar"><?php echo __('avatar'); ?>:</label>
				<div class="upload">
					<input type="text" id="avatar" name="avatar" size="35" readonly="readonly" class="inText" />
					<input id="btnFilePos" class="btnForm" type="button" value="<?php echo __('choose'); ?>" name="fake"/>
					<input id="avatar_f" class="avatar_f" type="file" onchange="javascript: document.getElementById('avatar').value = this.value" name="avatar_f" size="36" />
				</div>
			</div>

			<div class="group">
				<p><?php echo __('file info'); ?></p>
		    </div>
		    <div class="group">
		    	<p><?php echo __('math info'); ?></p>		
		    </div>
		    
		    <div class="group">
		    	<label for="captcha"><?php echo $captchaTxt; ?> <?php echo __('is'); ?></label>
		    	<input type="text" id="captcha" name="captcha" size="2" maxlength="2" class="inTextSmall" />
		    </div>
			 
		    <div class="group">
		    	<button type="submit" name="ok" class="btnForm"><?php echo __('register action'); ?></button>
		    </div>	    
		    
		</div>
    </form>

<script type="text/javascript">
// <![CDATA[
    $(document).ready(function() {
	var form = $('#registerForm');
	form.submit(function(){
	    if (validateLogin() & validatePass() & validatePasswords() & validateEmail() & validateCaptcha()){
		//return true;
	    } else {
		//return true;
	    }
	});
	
	$('#login').blur(validateLogin);
	function validateLogin(){
	    var value = $('#login').val();
	   
	    if (value.length < 5){
		$('#login').addClass('inError');
		$('#loginError').addClass('msgError').text('<?php echo __('error min length login'); ?>');
		return false;
	    } else {
		$('#login').removeClass('inError');
		$('#loginError').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#password').blur(validatePass);
	function validatePass(){
	    var value = $('#password').val();
	    if (value.length < 9 || value.length > 50){
		$('#password').addClass('inError');
		$('#passwordError').addClass('msgError').text('<?php echo __('error min length password'); ?>');
		return false;
	    } else {
		validatePasswords();
		$('#password').removeClass('inError');
		$('#passwordError').removeClass('msgError').text('');
		return true;
	    } 
	}
	
	$('#password_confirm').blur(validatePasswords);
	function validatePasswords(){
	    if ($('#password').val() != $('#password_confirm').val()){
		$('#password_confirm').addClass('inError');
		$('#passwordConfirmError').addClass('msgError').text('<?php echo __('error passwords dont match'); ?>');
		return false;
	    } else {
		$('#password_confirm').removeClass('inError');
		$('#passwordConfirmError').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#email').blur(validateEmail);
	function validateEmail(){
	    var exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
	    var email = $("#email").val();
	    if (!exp.test(email)){
		$('#email').addClass('inError');
		$('#emailError').addClass('msgError').text('<?php echo __('error incorrect email'); ?>');
		return false;
	    } else {
		$('#email').removeClass('inError');
		$('#emailError').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#captcha').blur(validateCaptcha);
	function validateCaptcha(){
	    var value = $('#captcha').val();
	    if (value == ''){
		$('#captcha').addClass('inError');
		return false;
	    } else {
		$('#captcha').removeClass('inError');
		return true;
	    }
	}
	
    });
// ]]>	
</script>

<?php
}


//formularz dodania wątku
if ($addTopicForm){
    echo '<a name="watki" id="watki"></a>';
    echo '<h2>' . $pageName . '</h2>';  

    
    ?>

    <form name="formAddTopic" id="formAddTopic" class="" method="post" action="<?php echo $url; ?>,dodaj#watki">
		<div class="form">
		    <?php
		    echo $message;
		    ?>
		    <h3><?php echo __('add new topic'); ?></h3>
		    
		    <div class="txt_com"><?php echo $topicName; ?></div>
		    
		    <?php
		    if ($forumLogged){
		    ?>    
		    <input type="hidden" name="author" value="<?php echo $who; ?>" />
		    
		    <?php
		    } else {
		    ?>
		    
		    <div class="group">
		    	<label for="topicAuthor"><?php echo __('author'); ?>:</label>
		    	<input type="text" id="topicAuthor" name="author" class="inText inLong" size="35" maxlength="50" value="<?php echo $author; ?>" />
		    </div>
		    
		    <?php
		    }
		    ?>
		    
		    <div class="group">
		    	<label for="topicTitle"><?php echo __('title'); ?>:</label>
		    	<input type="text" id="topicTitle" class="inText inLong" name="title" size="35" maxlength="50" value="<?php echo $title; ?>" />
		    </div>
		    
		    <div class="group">
		    	<label for="topicContent"><?php echo __('content'); ?>:</label>
		    	<textarea id="topicContent" name="content" rows="8" cols="40" class="inTextArea inLong"><?php echo $content; ?></textarea>
		    </div>
		    
		    <div class="group">
		    	<p><?php echo __('math info'); ?></p>		
		    </div>
		    
		    <input type="hidden" name="id_topic" value="<?php echo $topicId; ?>" />
		    
		    <div class="group">
		    	<label for="topicCaptcha"><?php echo $captchaTxt; ?> <?php echo __('is'); ?></label>
		    	<input type="text" id="topicCaptcha" name="captcha" size="2" maxlength="2" class="inTextSmall" />
		    </div>
			 
		    <div class="group">
		    	<button type="submit" name="ok"><?php echo __('add action'); ?></button>
		    </div>
		    
		</div>
    </form>

<script type="text/javascript">
// <![CDATA[
	$(document).ready(function() {
	    var form = $('#formAddTopic');
	    form.submit(function(){
		if (validateAuthor() & validateTitle() & validateContent() & validateCaptcha()){
		    //return true;
		} else {
		   //return true;
		}
	    });
	    
	    $('#topicAuthor').blur(validateAuthor);
	    function validateAuthor(){
		var value = $('#topicAuthor').val();
		if (value == ''){
		    $('#topicAuthor').addClass('inError');
		    return false;
		} else {
		    $('#topicAuthor').removeClass('inError');
		    return true;
		}
	    }
	    
	    $('#topicTitle').blur(validateTitle);
	    function validateTitle(){
		var value = $('#topicTitle').val();
		if (value == ''){
		    $('#topicTitle').addClass('inError');
		    return false;
		} else {
		    $('#topicTitle').removeClass('inError');
		    return true;
		}
	    }
	    
	    $('#topicContent').blur(validateContent);
	    function validateContent(){
		var value = $('#topicContent').val();
		if (value == ''){
		    $('#topicContent').addClass('inError');
		    return false;
		} else {
		    $('#topicContent').removeClass('inError');
		    return true;
		}
	    }

	    $('#topicCaptcha').blur(validateCaptcha);
	    function validateCaptcha(){
		var value = $('#topicCaptcha').val();
		if (value == ''){
		    $('#topicCaptcha').addClass('inError');
		    return false;
		} else {
		    $('#topicCaptcha').removeClass('inError');
		    return true;
		}
	    }
	    
	});
 // ]]>	
</script>   
    <?php
}

//formularz dodania odpowiedzi
if ($addRespondForm){
    echo '<a name="odpowiedz" id="odpowiedz"></a>';
    
    echo '<h2>' . $pageName . '</h2>';
    
    ?>
    <form name="formAddPost" id="formAddPost" class="" method="post" action="<?php echo $url; ?>,odpowiedz#odpowiedzi">
		<div class="form">
		<?php
		echo $message;
		?>
		    <h3><?php echo __('add new post'); ?></h3>
		    
		    <div class="txt_com"><?php echo $topicName; ?></div>
		    
		    <?php
		    if ($forumLogged){
		    ?>
		    <input type="hidden" name="author" value="<?php echo $who; ?>" />
		    <?php
		    } else {
		    ?>
		    <div class="group">
		    	<label for="postAuthor"><?php echo __('author'); ?>:</label>
		    	<input type="text" id="postAuthor" name="author" class="inText inLong" size="35" maxlength="50" value="<?php echo $author; ?>" />
		    </div>
		    <?php
		    }
		    ?>
		    
		    <div class="group">
		    	<label for="postContent"><?php echo __('content'); ?>:</label>
		    	<textarea id="postContent" name="content" rows="8" cols="40" class="inTextArea inLong"><?php echo $cita . $content; ?></textarea>
		    </div>
		    
		    <div class="group">
		    	<p><?php echo __('math info'); ?></p>		
		    </div>
		    
		    <input type="hidden" name="id_topic" value="<?php echo $topicId; ?>" />
		    
		    <div class="group">
		    	<label for="postCaptcha"><?php echo $captchaTxt; ?> <?php echo __('is'); ?></label>
		    	<input type="text" id="postCaptcha" name="captcha" size="2" maxlength="2" class="inText" />
	    	</div>
			 
		    <div class="group"><button type="submit" name="ok" class="btnForm"><?php echo __('respond action'); ?></button></div>
		    
		</div>
    </form>


<script type="text/javascript">
// <![CDATA[
	$(document).ready(function() {
	    var form = $('#formAddPost');
	    form.submit(function(){
		if (validateAuthor() & validateContent() & validateCaptcha()){
		    //return true;
		} else {
		    //return true;
		}
	    });
	    
	    $('#postAuthor').blur(validateAuthor);
	    function validateAuthor(){
		var value = $('#postAuthor').val();
		if (value == ''){
		    $('#postAuthor').addClass('inError');
		    return false;
		} else {
		    $('#postAuthor').removeClass('inError');
		    return true;
		}
	    }
	    
	    $('#postContent').blur(validateContent);
	    function validateContent(){
		var value = $('#postContent').val();
		if (value == ''){
		    $('#postContent').addClass('inError');
		    return false;
		} else {
		    $('#postContent').removeClass('inError');
		    return true;
		}
	    }

	    $('#postCaptcha').blur(validateCaptcha);
	    function validateCaptcha(){
		var value = $('#postCaptcha').val();
		if (value == ''){
		    $('#postCaptcha').addClass('inError');
		    return false;
		} else {
		    $('#postCaptcha').removeClass('inError');
		    return true;
		}
	    }
	    
	});
 // ]]>	
</script>

    <?php
}


if ($showTopics){

    echo '<h2>' . $pageName . '</h2>';
    echo $message;
    
    if (!$forumLogged){
	?>
	<ul class="forum__actions">
	    <li><a href="forum,logowanie" class="button-wrapper"><?php echo __('login action'); ?></a></li>
	    <li><a href="forum,rejestracja" class="button-wrapper"><?php echo __('register action'); ?></a></li>
	</ul>
	<?php
    }
    
    
    if ($topicId != 0){
	echo '<div class="topicText">';
	echo '<div class="topicAuthor">';
	// echo '<span class="forumAvatarFrame"></span>';
	echo '<img class="forumAvatarImage" src="' . $topicContent['avatar'] . '" alt="' . $topicContent['alt'] . '" />';
	echo '<span class="topicDate">' . $topicContent['date'] . '</span>';
	echo '<span class="topicHour">' . $topicContent['hour'] . '</span>';		
	echo '<span class="author">' . $topicContent['author'] . '</span>';
	echo '</div>';
	
	echo '<div class="topicContent">' . nl2br($topicContent['content']);
	if ($addTopic){
	    echo '<ul class="respondButtonList">';
	    echo '<li><a href="forum,o,' . $topicId . ',0,' . $urlAddPost . '#odpowiedz" class="button-wrapper">' . __('respond action') . '</a></li>';
	    echo '<li><a href="#odpowiedzi" class="button-wrapper">' . __('show responds') .'</a>';
	    echo '</ul></div>';
	}
	echo '</div>';
	
    }
    ?>

    <a name="watki" id="watki"></a>
    <h3 class="subHead"><?php echo __('topics'); ?></h3>
    
    <?php
    //wątki
    if ($numTopics > 0){
    ?>
	<?php
	if ($msgTopic){
	    echo $message;
	}
	?>
	<div class="forum__topics--holder">
	<table class="forum__topics">
    <thead>
		<tr>
			<th class="titles"><?php echo __('title'); ?></th>
			<th class="topics"><?php echo __('topics'); ?></th>
			<th class="answers"><?php echo __('posts'); ?></th>
		</tr>
	</thead>
	<tbody>
	<?php
	foreach ($arrTopics as $value){
	    echo '<tr>';
	    echo '<td><a href="forum,s,' . $value['id_topic'] . ',' . trans_url_name($value['topic']) . ' class="forum__topics--link"><span>' . $value['topic'] . '</span><p>' . $value['content'] . '</p></a></td>';
	    echo '<td>' . $value['numTopics'] . '</td>';
	    echo '<td>' . $value['numPosts'] . '</td>';
	    echo '</tr>';
	}
	?>
	</tbody>
	
	</table>
	</div>
    <?php
    } else {
	echo '<p>' . __('no topics') . '</p>';
    }
    if ($addTopic){
	if ($topicId == 0){
	    echo '<a href="forum,d,' . $topicId . ',' . $urlAdd . '#watek" class="button-wrapper">' . __('add new main topic') . '</a>';
	} else {
	    echo '<a href="forum,d,' . $topicId . ',' . $urlAdd . '#watek" class="button-wrapper">' . __('add new current topic') . '</a>';
	}
    }
    ?>
	
    <?php
    if ($topicId != 0){
    ?>
	
    <a name="odpowiedzi" id="odpowiedzi"></a>
    <h3 class="subHead"><?php echo __('posts'); ?></h3>    
    
    <?php
    //posty
    if ($numPosts > 0 ){
    ?>
	<?php
	if ($msgPost){
	    echo $message;
	}
	?>    
    
	<ul class="listPosts">
	<?php
	$n = 1;
	foreach ($arrPosts as $value){

	    echo '<li>';
	    //echo '<div class="postNumber">' . $n . '.</div>';
	    echo '<div class="postText">';
	    
	    
	    echo '<div class="postAuthor">';
	    // echo '<span class="forumAvatarFrame"></span>';
	    echo '<img class="forumAvatarImage" src="' . $value['avatar'] . '" alt="' . $value['alt'] . '" />';	    
	    echo '<span class="postDate">' . $value['date'] . '</span>';
	    echo '<span class="postHour">' . $value['hour'] . '</span>';

	    
	    echo '<span class="author">' . $value['author'] . '</span>';
	    echo '</div>';

	    echo '<div class="postContent">' . nl2br($value['content']);
	    echo '<ul class="respondButtonList">';
	    echo '<li><a href="forum,o,' . $topicId . ',0,' . $urlAddPost . '#odpowiedz" class="button-wrapper">' . __('respond action') . '</a></li>';
	    echo '<li><a href="forum,o,' . $topicId . ',' . $value['id_posts'] . ',' . $urlAddCita . '#odpowiedz" class="button-wrapper">' . __('cite and respond action') . '</a></li>';
	    echo '</ul></div>';
	    echo '</div>';
	    echo '</li>';
	    $n++;
	    
	}
	?>
	</ul>

    <?php
    } else {
	echo '<p>' . __('no posts') . '</p>';
    }
    
    }
}
?>
