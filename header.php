<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<!--[if lte IE 8]>
<script type="text/javascript">
    window.location = "<?php echo $pathTemplate?>/ie8.php";
</script>
<![endif]-->
<head>
<title><?php echo $pageTitle; ?></title>
<link href="https://fonts.googleapis.com/css?family=Francois+One&subset=latin-ext" rel="stylesheet">
<meta name="description" content="<?php echo $pageDescription; ?>" />
<meta name="keywords" content="<?php echo $pageKeywords; ?>" />
<meta name="author" content="<?php echo $cmsConfig['cms']; ?>" />
<meta name="revisit-after" content="3 days" />
<meta name="robots" content="all" />
<meta name="robots" content="index, follow" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8" />

<?php
$pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
foreach ($js as $k => $v)
{
    echo '<script type="text/javascript" src="'. $pathTemplate .'/js/' . $v . '"></script>' . "\r\n";
}
echo '<script src="'. $pathTemplate .'/js/owl.carousel.min.js"></script>' . "\r\n";
echo '<script src="'. $pathTemplate .'/vendor/bootstrap/js/bootstrap.min.js"></script>' . "\r\n";
echo '<script src="'. $pathTemplate .'/vendor/modernizr/modernizr-custom.js"></script>' . "\r\n";
echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/vendor/bootstrap/css/bootstrap.min.css"/>' . "\r\n";
foreach ($css as $k => $v)
{
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/css/' . $v . '"/>' . "\r\n";
    if ($v == 'style.css')
    {
    $popupBackground = '#fff';
        $mainColor = $templateConfig['mainColor'];
        $overlayColor = $templateConfig['overColor'];
        $highColor = $templateConfig['highColor'];
            
    // wersja zalobna
    if ($outSettings['funeral'] == 'włącz') 
    {
        $popupBackground = $templateConfig['popupBackground-bw'];
        $mainColor = $templateConfig['mainColor-bw'];
        $overlayColor = $templateConfig['overColor-bw'];
        $highColor = $templateConfig['highColor-bw'];
        echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/style.css"/>' . "\r\n";
    }       
    }
        
    if ($v == 'jquery.fancybox.css')
    {
        if ($outSettings['funeral'] == 'włącz') 
        {
        echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/jquery.fancybox.css"/>' . "\r\n";
    }
    }
    if ($v == 'addition.css')
    {
    if ($outSettings['funeral'] == 'włącz')
    {
        echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/addition.css"/>' . "\r\n";
    }
    }      
}

$contrast = '';
if ($_SESSION['contr'] == 1)
{
    $contrast = 'flashvars.contrast = 1;' . "\r\n";
    $popupBackground = $templateConfig['popupBackground-ct'];
    $mainColor = $templateConfig['mainColor-ct'];
    $overlayColor = $templateConfig['overColor-ct'];
    $highColor = $templateConfig['highColor-ct'];	    
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/style.css"/>' . "\r\n";
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/jquery.fancybox.css"/>' . "\r\n";
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/addition.css"/>' . "\r\n";
    // echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/css/owl.carousel.css"/>' . "\r\n";
}	
	
echo '<link rel="shortcut icon" href="http://' . $pageInfo['host'] . '/' . $templateDir . '/images/favicon.ico" />' . "\r\n";


?>
<script type="text/javascript">
// <![CDATA[
    template = {
	overlayColor : '<?php echo $overlayColor; ?>'
    }
// ]]>
</script>
<script type="text/javascript">
    texts = {
        searchQuery: 'Wyszukiwana fraza',
        image: 'Obraz',
        enlargeImage: 'Powiększ obraz',
        closeGallery: 'Zamknij powiększenie',
        prevGallery: 'Poprzedni obraz',
        nextGallery: 'Następny obraz',
        errorLoginMin: 'Minimalna długość loginu to 4 znaki',
        errorPasswordMin: 'Minimalna długość hasła to 8 znaków',
        errorIncorrectEmail: 'Nieprawidłowy adres e-mail',
        errorFirstname: 'Wpisz swoje imię',
        errorLastname: 'Wpisz swoje nazwisko',
        errorPasswordDontMatch: 'Hasła nie zgadzają się',
        errorTopicAuthor: 'Wpisz autora wątku',
        errorTopicTitle: 'Wpisz tytuł wątku',
        errorTopicContent: 'Wpisz treść wątku',
        errorPostAuthor: 'Wpisz autora odpowiedzi',
        errorPostContent: 'Wpisz treść odpowiedzi'
    };
        settings = {
        overlayColor: '<?php echo $overlayColor; ?>',
        transition: '<?php echo $outSettings['animType']?>',
        animationDuration: <?php echo $outSettings['transition']?>,
        duration: <?php echo $outSettings['duration']?>,
        showClock: false
    };
</script>
<script>
    $(document).ready(function() {
        $('.hero__carousel > div').owlCarousel({
            singleItem: true,
            autoPlay: 1e3 * settings.duration,
            slideSpeed: 1e3 * settings.animationDuration,
            paginationSpeed: 1e3 * settings.animationDuration,
            transitionStyle: settings.transition,
            pagination: false
        });
    });
</script>
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-3924208-1']);
	_gaq.push(['_setDomainName', '.szkolnastrona.pl']);
	_gaq.push(['_setAllowHash', false]);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
</head>
<body>