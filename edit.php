<?php
if ($changeAvatar){
?>
<script type="text/javascript">
// <![CDATA[
    $(document).ready(function() {
	$('.userAvatar img').attr('src', 'files/<?php echo $lang; ?>/avatars/mini/<?php echo $avatar; ?>');		
    });
// ]]>	
</script>
<?php
}
if ($deleteAvatar){
?>
<script type="text/javascript">
// <![CDATA[
    $(document).ready(function() {
	$('.userAvatar img').attr('src', 'files/avatar-<?php echo $sex; ?>.png');		
    });
// ]]>	
</script>
<?php
}

?>

<a name="edytuj" id="edytuj"></a>
<?php
echo '<h2>' . $pageName . '</h2>';

if ($showEditForm){
?>
    <form id="editForm" name="editForm" class="jNice" method="post" action="<?php echo $url; ?>,aktualizuj#edytuj" enctype="multipart/form-data">
		<div class="form">
		    <?php
		    echo $message;
		    ?>
		    <h3><?php echo __('edit action'); ?></h3>
		    
		    <div class="group">
		    	<label><?php echo __('login'); ?>:</label>
		    	<input type="text" readonly value="<?php echo $outRow['login']; ?>" />
		    </div>
		    
		    <div class="group">
		    	<label for="password"><?php echo __('password'); ?>:</label>
		    	<input type="password" id="password" name="password" class="inText" size="35" maxlength="50" value="<?php echo $password; ?>" />
		    	<span id="passwordError" class="msgMarg"></span>
		    </div>
		    
		    <div class="group">
		    	<label for="password_confirm"><?php echo __('repeat password'); ?>:</label>
		    	<input type="password" id="password_confirm" name="password_confirm" class="inText" size="35" maxlength="50" value="<?php echo $password_confirm; ?>" />
		    	<span id="passwordConfirmError" class="msgMarg"></span>
		    </div>
		    
		    <div class="group">
		    	<label for="email"><?php echo __('email'); ?>:</label>
			    <input type="text" id="email" name="email" class="inText" size="35" maxlength="100" value="<?php echo $email; ?>" />
			    <span id="emailError" class="msgMarg"></span>
			</div>
		    
		    <div class="group">
		    	<label for="first_name"><?php echo __('firstname'); ?>:</label>
		    	<input type="text" id="first_name" name="first_name" class="inText" size="35" maxlength="50" value="<?php echo $first_name; ?>" />
		    </div>
		    
		    <div class="group">
		    	<label for="last_name"><?php echo __('lastname'); ?>:</label>
		    	<input type="text" id="last_name" name="last_name" class="inText" size="35" maxlength="50" value="<?php echo $last_name; ?>" />
		    </div>
		    
		    <div class="group">
		    	<label><?php echo __('gender'); ?>:</label>
			    <div class="radio">
					<input type="radio" id="sex_m" name="sex" value="m" <?php if ($sex == 'm'){ echo 'checked="checked"';} ?> />&nbsp;<label for="sex_m" id="l_sex_m"><span class="radio"></span><span class="label"><?php echo __('man'); ?></span></label>
				</div>
				<div class="radio">
					<input type="radio" id="sex_f" name="sex" value="f" <?php if ($sex == 'f'){ echo 'checked="checked"';} ?> />&nbsp;<label for="sex_f" id="l_sex_f"><span class="radio"></span><span class="label"><?php echo __('woman'); ?></span></label>
			    </div>
			</div>
		    
		    <?php
		    if ($avatar != ''){
		    ?>
		    <input type="hidden" name="avatar" value="<?php echo $avatar; ?>" />
		    <div class="group">
		    	<label><?php echo __('avatar'); ?>:</label>
		   		 <div class="formR p10"><span class="avatarFrameEdit"></span><img src="files/<?php echo $lang; ?>/avatars/mini/<?php echo $avatar; ?>" alt="<?php echo __('avatar'); ?>" class="avatarFrameImage" /><a href="<?php echo $url; ?>,usun" title="Usuń avatar" class="delete"><span class="sr-only"><?php echo __('delete avatar'); ?></span></a>
		   		 </div>
		    </div>
		    <?php
		    } else {
		    ?>
		    
		    <div class="group">
		    	<label for="avatar"><?php echo __('avatar'); ?>:</label>			
				<input type="text" id="avatar" name="avatar" size="35" readonly="readonly" class="inText" />
				<div class="upload">
					<input id="btnFilePos" class="btnForm" type="button" value="Wybierz..." name="fake"/>
					<input id="avatar_f" class="avatar_f" type="file" onchange="javascript: document.getElementById('avatar').value = this.value" name="avatar_f" size="36" />
				</div>
				<div class="group">
					<p><?php echo __('file info'); ?></p>
				</div>
		    </div>

		    
		    <?php
		    }
		    ?>
		    
		    <div class="group">
		    	<button type="submit" name="ok" class="btnForm"><?php echo __('update action'); ?></button>
		    </div>	    
		    
		</div>
    </form>

<script type="text/javascript">
// <![CDATA[
    $(document).ready(function() {
	var form = $('#editForm');
	form.submit(function(){
	    if (validatePass() & validatePasswords() & validateEmail()){
		//return true;
	    } else {
		//return false;
	    }
	});
	
	$('#password').blur(validatePass);
	function validatePass(){
	    var value = $('#password').val();
	    if (value.length > 0){
		if (value.length < 9 || value.length > 50){
		    $('#password').addClass('inError');
		    $('#passwordError').addClass('msgError').text('<?php echo __('error min length password'); ?>');
		    return false;
		} else {
		    validatePasswords();
		    $('#password').removeClass('inError');
		    $('#passwordError').removeClass('msgError').text('');
		    return true;
		}
	    } else {
		    $('#password').removeClass('inError');
		    $('#passwordError').removeClass('msgError').text('');
		    return true;		
	    }
	}

	$('#password_confirm').blur(validatePasswords);
	function validatePasswords(){
	    if ($('#password').val() != $('#password_confirm').val()){
		$('#password_confirm').addClass('inError');
		$('#passwordConfirmError').addClass('msgError').text('<?php echo __('error passwords dont match'); ?>');
		return false;
	    } else {
		$('#password_confirm').removeClass('inError');
		$('#passwordConfirmError').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#email').blur(validateEmail);
	function validateEmail(){
	    var exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
	    var email = $("#email").val();
	    if (!exp.test(email)){
		$('#email').addClass('inError');
		$('#emailError').addClass('msgError').text('<?php echo __('error incorrect email'); ?>');
		return false;
	    } else {
		$('#email').removeClass('inError');
		$('#emailError').removeClass('msgError').text('');
		return true;
	    }
	}	
    });
// ]]>	
</script>	
<?php
} else {
    echo $message;
}
?>

