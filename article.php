<h2><?php echo $pageName?></h2>
<?php
echo $message;

if ($showLoginForm)
{
    include( CMS_TEMPL . DS . 'form_login.php');
}
		
if ($showArticle)
{
    echo '<div class="leadArticle">' . $article['lead_text'] . '</div>';
    
    echo $article['text'];
		
    if (! check_html_text($article['author'], '') )
    {
	?>
	<div class="authorName"><?php echo __('author'); ?>: <?php echo $article['author']?></div>
	<?php
    }
				
    /*
     *  Wypisanie plikow do pobrania
     */
    if ($numFiles > 0)
    {
	?>
	<div class="filesWrapper">
	    <h3 class="filesHead"><?php echo __('files'); ?></h3>
	    <ul>
	    <?php
	    foreach ($outRowFiles as $row)
            {
		$target = 'target="_blank" ';
				
		if (filesize('download/'.$row['file']) > 5000000)
		{
		    $url = 'download/'.$row['file'];
		} else
		{
		    $url = 'index.php?c=getfile&amp;id='.$row['id_file'];
		}
		if (trim($row['name']) == '')
		{
		    $name = $row['file'];
		} else
		{
		    $name = $row['name'];
		}			
		$size = file_size('download/'.$row['file']);	
		?>
		<li>
		    <h4>
			<a href="<?php echo $url?>" <?php echo $target?>><?php echo $name?></a> <span>(<?php echo $size?>)</span>
		    </h4>
		</li>
		<?php
	    }
	    ?>
	    </ul>
	</div>
    <?php
    }
		
    /*
     *  Wypisanie zdjec
     */
    if ($numPhotos > 0)
    {	
	$i = 0;
	?>
	<ul class="gallery">
	    <?php
	    foreach ($outRowPhotos as $row)
            {
		$i++;
		$noMargin = '';
		if ($i == $pageConfig['zawijaj'])
		{
		    $noMargin = ' noMargin';
		}
		?>
		<li class="photoWrapperGallery">
		    <a href="files/<?php echo $lang?>/<?php echo $row['file']?>" data-fancybox-group="gallery" rel="fancybox" title="<?php echo $row['name']?>" class="photo"><img src="files/<?php echo $lang?>/mini/<?php echo $row['file']?>" alt="Powiększ zdjęcie" /></a>
		    <?php
		    if (! check_html_text($row['name'], '') )
		    {
			?>
			<p><?php echo $row['name']?></p>
			<?php
		    }
		    ?>
		</li>
		<?php
		if ($i == $pageConfig['zawijaj'])
		{
		    $i = 0;
		    ?>
		    <?php
		}						
	    }
	    ?>
	</ul>
    <?php
    }		
    if ($outSettings['pluginTweet'] == 'włącz')
    {
	 echo '<div class="Tweet"><iframe frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html" style="width:80px; height:30px;"></iframe></div>';  
    }

    if ($outSettings['pluginFB'] == 'włącz')
    {
	$fb_url = urlencode('http://'.$pageInfo['host'].'/index.php?c=article&amp&id='. $_GET['id']);
	echo '<div class="FBLike"><iframe src=\'http://www.facebook.com/plugins/like.php?href='.$fb_url.'&amp;layout=standard&amp;show_faces=true&amp;width=400&amp;action=like&amp;font=tahoma&amp;colorscheme=light&amp;height=32&amp;show_faces=false\' scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:32px;"></iframe></div>';   
    }
}
?>